## Canvas Image Draw 외 TweenMax, Scrollmagic 적용

산출물: [./scroll-canvas-draw.html](./scroll-canvas-draw.html)

### 화면/브라우저 대응

- 화면: PC용 페이지
- 브라우저 대응: IE11 이상 모던브라우저 지원

### `canvas` 에 img 넣었는데 안보인다?
```javascript
this.cardCanvas = this.obj.find('canvas');
this.cardContext = this.cardCanvas.get(0).getContext('2d');

this.cardContext.drawImage(this.sceneImages[0], 0, 0);
```
이미지 리소스가 load 되기전에 drawImage를 해버려서 그렇다. 아래처럼 이미지는 먼저 load해주고 load된 후에 drawImage 해야한다.

```javascript
for (let i = 0; i < totalImagesCount; i++) {
  let imgElem = new Image();
  imgElem.src = `../video/002/IMG_${7027 + i}.JPG`;
  this.sceneImages.push(imgElem);
}
window.addEventListener('load', function() {
  this.cardContext.drawImage(this.sceneImages[0], 0, 0);
});
```
`window.load` 는 한번 윈도우 로드되면 다시 이벤트가 실행 안되어서,, 반응형 대응으로 resize 될 때 추가로 리소스 로드해야 할 경우 `imgElem.onload` 방식으로 바꿔주면 됨.<br>(검색 키워드 : javascript 비동기 image load)

---

### 이미지 Blur 효과 - IE 11 대응

레이어팝업이 열릴 때 딤드 뒤 이미지에 Blur 효과를 요구한다면?

```javascript
<span class="content_img_filter_polyfill">
  <svg preserveAspectRatio="xMidYMin slice" viewBox="0 0 1004 660">
    <defs>
      <filter id="blur_grid1">
        <feGaussianBlur in="SourceGraphic" stdDeviation="3"></feGaussianBlur>
      </filter>
    </defs>
    <image href="/assets/pad/pad-0.jpg" width="100%" height="100%" filter="url(#blur_grid1)"></image>
  </svg>
</span>
```

CSS filter, canvas filter 둘 다 IE 에서는 지원을 안한다. SVG filter만 사용이 가능하다.<br>
https://caniuse.com/?search=filter

**blur 효과를 줄 수 있는 방법**

1. blur 적용할 요소나 그룹을 감싸는 wrapper 요소에 [CSS] `filter: blur(3px);`
    - 브라우저지원 : IE 11 이하 미지원
1. blur 적용할 요소를 덮는 레이어 요소에 [CSS] `backdrop-filter: blur(3px);`
    - 제한사항 : 레이어 요소는 background-color의 alpha 값이 100% 보다 작아야 함. (뒤가 비쳐야 함)
    - 브라우저 지원 : IE 11 이하 미지원, 파이어폭스 미지원
1. 특정 이미지 1장 url을 가지고 SVG 요소로 만들어 filter 적용
    - 제한사항 : url이 있는 리소스만 적용 가능. CSS로 그린 것은 적용 불가(?)
    - 브라우저 지원 : IE 9 이하 미지원
1. [JS] canvasRenderingContext2D.filter
    - 브라우저 지원 : IE 11 이하 미지원, 사파리 미지원

> 브라우저 지원 스펙상 IE11을 대응하려면 3번 SVG filter 만 가능.

**최종 정리**

[CSS] filter: blur(3px); 을 지원하지 않는 브라우저(IE11)는 JS에서 blur 용 SVG dom 추가

- 한계점1 : 이미지 리소스를 사용하지 않고 CSS로 그린 비주얼 이라면 적용 불가
- 한계점2 : 재생되는 이미지 -> 재생완료된 시점 이미지로 바로 바뀜

---

### 그 외 작업하며 참고한 레퍼런스

**TweenMax, TimelineMaxm (v2)**
- https://greensock.com/docs/v2

**ScrollMagic Demo, API**
- https://scrollmagic.io/examples/index.html
- https://scrollmagic.io/docs/index.html

**SVG요소를 createElement 할 땐 `document.createElementNS` 를 써야한다**
- https://developer.mozilla.org/ko/docs/Web/API/Document/createElementNS

**JS에서 CSS 스타일 값 얻기**
- https://zellwk.com/blog/css-values-in-js/
- https://developer.mozilla.org/ko/docs/Web/API/Window/getComputedStyle

**CSS 속성을 지원하는지 JS로 체크하기**
- https://stackoverflow.com/questions/36191797/how-to-check-if-css-value-is-supported-by-the-browser
- https://code.tutsplus.com/tutorials/quick-tip-detect-css3-support-in-browsers-with-javascript--net-16444