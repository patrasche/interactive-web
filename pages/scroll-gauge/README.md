## 스크롤 위치를 진행률 게이지(gauge)로 표현

산출물: [./scroll-gauge.html](./scroll-gauge.html)

### 화면/브라우저 대응

- 화면: PC용 페이지 (Mobile 은 깨지지 않을 정도로만..)
- 브라우저 대응: IE11 이상 모던브라우저 지원

### 현재 스크롤 위치를 %로

```javascript
nScrollPercent = (window.pageYOffset / ($(".container")[0].offsetHeight - window.innerHeight)) * 100;
```

> 현재 스크롤 위치 = 현재 scrollTop position / (컨테이너 전체 height - 현재 viewport(창:window) height)

컨테이너 전체 높이에서 viewport의 높이를 한 번 빼주는 것이 필요하다.

### 스크롤 이벤트 throttle

```javascript
if (!scrollTimer) {
  scrollTimer = setTimeout(function () {
    scrollTimer = null;
    // onScroll Action
  }, 50);
}
```

throttle은 성능 개선을 위한 기법 중 하나로 이벤트의 주기를 덜 빈번하게 만들 수 있다. 위의 코드는 개념을 테스트 해보기 위해 간단히 작성된 코드일뿐이다.
