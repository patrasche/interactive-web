var patrasche = patrasche || {};
patrasche.Cardunit = patrasche.Cardunit || {};

patrasche.Cardunit.CanvasLoad = function (container, opts) {
	var defOpts = {
		canvasFrameLengthData: "frame-length",
		canvasImgData: {
			PC: "pc-img",
			MO: "mo-img",
		},
		frameSrcData: {
			PC: "pc-frame-src",
			MO: "mo-frame-src",
		},
		viewType: "PC",
		on: {
			complete: null,
		},
	};
	this.opts = $.extend(defOpts, opts || {});
	if (!(this.welContainer = $(container)).length) return;
	this.init();
};
patrasche.Cardunit.CanvasLoad.prototype = {
	init: function () {
		this.initOpts();
		this.load();
	},
	initOpts: function () {
		this.targetLength = this.welContainer.length;
	},
	load: function () {
		var targetDoneCount = 0;
		var allTargetDone = $.proxy(function () {
			if (targetDoneCount === this.targetLength) {
				this.outCallback("complete");
			}
		}, this);
		var loadFrames = $.proxy(function (canvasElem) {
			var deferred = $.Deferred(),
				srcDataName = this.opts.frameSrcData[this.opts.viewType],
				frameLength = parseInt(canvasElem.data(this.opts.canvasFrameLengthData)),
				frameDoneCount = 0,
				pathData = "",
				pathname = "",
				filename = "",
				imgElem = null,
				imgObjList = [],
				allFrameDone = $.proxy(function () {
					if (frameDoneCount === frameLength) {
						canvasElem.data(this.opts.canvasImgData[this.opts.viewType], imgObjList);
						deferred.resolve();
					}
				}, this);

			if (!frameLength) {
				deferred.resolve();
			} else {
				pathData = canvasElem.attr("data-" + srcDataName);
				pathname = pathData.split("/");
				filename = pathname[pathname.length - 1];
				pathname = pathData.substring(0, pathData.lastIndexOf(filename));
				filename = filename.split(".");
				filename[0] = filename[0].substring(0, filename[0].lastIndexOf("-") + 1);

				for (var fIdx = 0, fMax = frameLength; fIdx < fMax; fIdx++) {
					imgElem = new Image();
					imgElem.onload = function () {
						frameDoneCount++;
						allFrameDone();
					};
					imgElem.src = pathname + filename[0] + fIdx + "." + filename[1];
					imgObjList.push(imgElem);
				}
			}
			return deferred.promise();
		}, this);

		for (var tIdx = 0, tMax = this.targetLength; tIdx < tMax; tIdx++) {
			var welCanvas = this.welContainer.eq(tIdx).find("canvas");
			loadFrames(welCanvas).done(
				$.proxy(function () {
					targetDoneCount++;
					allTargetDone();
				}, this)
			);
		}
	},
	outCallback: function (ing) {
		var callbackObj = this.opts.on[ing];
		if (callbackObj == null) return;
		callbackObj();
	},
};

patrasche.Cardunit.Blur = function (container, opts) {
	var defOpts = {
		blurRadiusPx: 3,
		imgFilterSelector: ".content_img_filter",
		jsCanvasSelector: ".js-canvas",
		svgWrapClass: "content_img_filter_polyfill",
		svgPreserveAspectRatioAttr: "xMidYMin slice",
		canvasImgData: {
			PC: "pc-img",
			MO: "mo-img",
		},
		viewType: "PC",
	};
	this.opts = $.extend(defOpts, opts || {});
	if (!(this.welContainer = $(container)).length) return;
	this.init();
};
patrasche.Cardunit.Blur.prototype = {
	init: function () {
		this.setElements();
		this.initOpts();
		this.buildSvgBlur();
	},
	setElements: function () {
		this.welJsCanvas = this.welContainer.find(this.opts.jsCanvasSelector);
	},
	initOpts: function () {
		var index = $(this.opts.imgFilterSelector).index(this.welContainer);
		this.filterId = "blur" + index;
		this.viewBox = {
			x: 0,
			y: 0,
			w: 0,
			h: 0,
		};
		this.domPolyfill = {
			PC: null,
			MO: null,
		};
	},
	removeSvgBlur: function () {
		var welSvgWrap = this.welContainer.find("." + this.opts.svgWrapClass);
		if (welSvgWrap.length) welSvgWrap.remove();
	},
	rebuildSvgBlur: function () {
		if (this.domPolyfill[this.opts.viewType] === null) {
			this.buildSvgBlur();
		} else {
			this.welContainer.append(this.domPolyfill[this.opts.viewType]);
		}
	},
	buildSvgBlur: function () {
		var elImg = null;
		var domPolyfill = document.createElement("span");
		var domSvg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
		var domDefs = document.createElementNS("http://www.w3.org/2000/svg", "defs");
		var domFilter = document.createElementNS("http://www.w3.org/2000/svg", "filter");
		var domGaBlur = document.createElementNS("http://www.w3.org/2000/svg", "feGaussianBlur");
		var domImage = document.createElementNS("http://www.w3.org/2000/svg", "image");

		if (this.welJsCanvas.length) {
			elImg = this.getDimmedImage("jsCanvas");
		}
		this.viewBox.w = elImg.naturalWidth;
		this.viewBox.h = elImg.naturalHeight;

		domPolyfill.setAttribute("class", this.opts.svgWrapClass);
		domSvg.setAttribute("preserveAspectRatio", this.opts.svgPreserveAspectRatioAttr);
		domSvg.setAttribute("viewBox", [this.viewBox.x, this.viewBox.y, this.viewBox.w, this.viewBox.h].join(" "));
		domFilter.setAttribute("id", this.filterId);
		domGaBlur.setAttribute("in", "SourceGraphic");
		domGaBlur.setAttribute("stdDeviation", this.opts.blurRadiusPx);
		domImage.setAttribute("href", elImg.src);
		domImage.setAttribute("width", "100%");
		domImage.setAttribute("height", "100%");
		domImage.setAttribute("filter", "url(#" + this.filterId + ")");

		domFilter.appendChild(domGaBlur);
		domDefs.appendChild(domFilter);
		domSvg.appendChild(domDefs);
		domSvg.appendChild(domImage);
		domPolyfill.appendChild(domSvg);

		this.domPolyfill[this.opts.viewType] = domPolyfill;
		this.welContainer.append(this.domPolyfill[this.opts.viewType]);
	},
	getDimmedImage: function (type) {
		var welImage = null;
		var canvasImgLength = 0;
		switch (type) {
			case "jsCanvas":
				welImage = this.welJsCanvas.find("canvas").data(this.opts.canvasImgData[this.opts.viewType]);
				canvasImgLength = welImage.length;
				welImage = welImage[canvasImgLength - 1];
				return welImage;
			default:
				return;
		}
	},
};

patrasche.Cardunit.svgIcon = function (container) {
	if (!(this.welContainer = $(container)).length) return;
	this.init();
};
patrasche.Cardunit.svgIcon.prototype = {
	init: function () {
		this.initOpts();
		this.buildLayout();
	},
	initOpts: function () {
		this.svgIcon = {};
		this.svgIcon["expand"] = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 383.36 383.36"><polygon class="arw1" points="60.587,353.067 167.893,245.76 137.6,215.68 30.293,322.987 39.68,301.653 39.68,244.48 0,244.053 0,383.36 138.88,383.36 139.093,343.68 80.853,343.68" /> <polygon class="arw2" points="244.053,0 244.48,39.68 301.653,39.68 322.987,30.293 215.68,137.6 245.76,167.893 353.067,60.587 343.68,80.853 343.68,139.093 383.36,138.88 383.36,0" /></svg>';
		this.svgIcon["close"] = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 402.56 402.56"><polygon class="arw1" points="38.613,234.88 38.4,274.56 96.64,274.56 116.693,265.173 9.6,372.48 39.68,402.56 147.2,295.253 137.813,316.587 137.813,373.76 177.493,374.187 177.493,234.88"/><polygon class="arw2" points="306.987,128.213 285.653,137.6 392.96,30.08 362.88,0 255.573,107.307 264.96,87.04 264.96,28.8 225.28,29.013 225.28,167.893 364.587,167.893 364.16,128.213"/><g>';
	},
	buildLayout: function () {
		for (var i = 0, max = this.welContainer.length; i < max; i++) {
			var target = this.welContainer.eq(i);
			var targetData = target.data("icon");

			target.append(this.svgIcon[targetData]);
		}
	},
};

patrasche.Cardunit.GlobalState = {
	OpenedLayerCount: 0,
	CurrentKeycode: null,
};
patrasche.Cardunit.LayerPopup = function (container, opts) {
	var defOpts = {
		cardInnerSelector: ".cardunit_inner",
		openerSelector: ".cardunit_content .cardunit_btn",
		closerSelector: ".cardunit_detail .cardunit_btn",
		dimmedSelector: ".dimmed",
		expandIconSelector: ".cardunit_ico.expand svg",
		closeIconSelector: ".cardunit_ico.close svg",
		layerSelector: ".cardunit_detail",
		layerTxtSelector: ".content_txt",
		learnBtnClass: "learn",
		styleAttr: {
			hideLayer: {
				display: "none",
			},
			showLayer: {
				display: "",
			},
			hideLayerTxt: {
				opacity: 0,
				transform: "translateY(20px)",
			},
			showLayerTxt: {
				opacity: 1,
				transform: "translateY(0)",
			},
		},
		classAttr: {
			active: "is-show",
			over: "is-hover",
			keyFocus: "is-keyfocus",
		},
		stateAttr: {
			opened: false,
		},
		time: {
			show: {
				dimmedDuration: null,
				duration: 1,
				eachDelay: 0.2,
			},
			hide: {
				dimmedDuration: null,
				duration: 0.5,
				eachDelay: 0.1,
			},
		},
	};
	this.opts = $.extend(defOpts, opts || {});
	if (!(this.welContainer = $(container)).length) return;
	this.init();
};
patrasche.Cardunit.LayerPopup.prototype = {
	init: function () {
		this.setElements();
		this.initOpts();
		this.initLayout();
		this.buildTween();
		this.bindEvents();
	},
	setElements: function () {
		this.welCardInner = this.welContainer.find(this.opts.cardInnerSelector);
		this.welBtnOpen = this.welCardInner.find(this.opts.openerSelector);
		this.welBtnClose = this.welCardInner.find(this.opts.closerSelector);
		this.welDimmed = this.welCardInner.find(this.opts.dimmedSelector);
		this.welLayer = this.welCardInner.find(this.opts.layerSelector);
		this.welLayerTxt = this.welLayer.find(this.opts.layerTxtSelector);
	},
	initOpts: function () {
		var transitionDuration = null;
		if (this.opts.time.show.dimmedDuration === null) {
			transitionDuration = parseFloat(getComputedStyle(this.welDimmed.get(0)).transitionDuration);
			$.extend(this.opts.time.show, {
				dimmedDuration: transitionDuration,
			});
		}
		if (this.opts.time.hide.dimmedDuration === null) {
			if (transitionDuration === null) {
				transitionDuration = parseFloat(getComputedStyle(this.welDimmed.get(0)).transitionDuration);
			}
			$.extend(this.opts.time.hide, {
				dimmedDuration: transitionDuration,
			});
		}
	},
	initLayout: function () {
		this.welLayer.css(this.opts.styleAttr.hideLayer);
		this.welLayerTxt.children().css(this.opts.styleAttr.hideLayerTxt);
		this.timelineLayer = new TimelineLite();
	},
	buildTween: function () {
		$.extend(this, {
			tween: {
				layerTxt: {
					show: $.proxy(function (target) {
						var targetIdx = target.index();
						var duration = this.opts.time.show.duration;
						var eachDelay = this.opts.time.show.eachDelay;
						TweenLite.killTweensOf(target.get(0));
						TweenLite.to(
							target.get(0),
							duration,
							$.extend(this.opts.styleAttr.showLayerTxt, {
								delay: eachDelay * targetIdx,
							})
						);
					}, this),
					hide: $.proxy(function (target) {
						var targetIdx = target.index();
						var duration = this.opts.time.hide.duration;
						var eachDelay = this.opts.time.hide.eachDelay;
						TweenLite.killTweensOf(target.get(0));
						TweenLite.to(
							target.get(0),
							duration,
							$.extend(this.opts.styleAttr.hideLayerTxt, {
								delay: eachDelay * (this.welLayerTxt.children().length - 1 - targetIdx),
							})
						);
					}, this),
				},
			},
		});
	},
	bindEvents: function () {
		$(window)
			.on("keydown", function (e) {
				patrasche.Cardunit.GlobalState.CurrentKeycode = e.keyCode;
			})
			.on("keyup", function () {
				patrasche.Cardunit.GlobalState.CurrentKeycode = null;
			});
		this.welCardInner.on("mouseenter focusin", $.proxy(this.onMouseenterObj, this)).on("mouseleave focusout", $.proxy(this.onMouseleaveObj, this));
		this.welLayer.on("focus", $.proxy(this.onFocusTarget, this)).on("blur", $.proxy(this.onBlurTarget, this));
		this.welBtnOpen.on("click", $.proxy(this.openLayer, this)).on("focus", $.proxy(this.onFocusTarget, this)).on("blur", $.proxy(this.onBlurTarget, this));
		this.welBtnClose.on("click", $.proxy(this.closeLayer, this)).on("focus", $.proxy(this.onFocusTarget, this)).on("blur", $.proxy(this.onBlurTarget, this));
	},
	bindOutsideEvents: function (type) {
		if (type) {
			$(document).on("click.layerpopup", $.proxy(this.onClickoutsideObj, this));
		} else {
			$(document).off("click.layerpopup");
		}
	},
	openLayer: function (e) {
		var layerTxtShowDuration = null;
		var keyIn = false;
		if (patrasche.Cardunit.GlobalState.CurrentKeycode != null) {
			keyIn = true;
		}

		e.preventDefault();
		if (this.opts.stateAttr.opened) return;
		this.opts.stateAttr.opened = true;
		patrasche.Cardunit.GlobalState.OpenedLayerCount++;

		// step1
		this.timelineLayer.clear().to(this.welLayer, 0, this.opts.styleAttr.showLayer);
		this.timelineLayer.to({}, this.opts.time.show.dimmedDuration, {
			onStart: $.proxy(function () {
				// step2
				this.welContainer.addClass(this.opts.classAttr.active);
				if (keyIn) {
					if (!this.welLayer.get(0).hasAttribute("tabindex")) {
						this.welLayer.attr("tabindex", "-1");
					}
					this.welLayer.focus();
				}
				this.bindOutsideEvents(true);
			}, this),
		});
		layerTxtShowDuration = this.opts.time.show.eachDelay * (this.welLayerTxt.children().length - 1) + this.opts.time.show.duration;
		this.timelineLayer.to({}, layerTxtShowDuration, {
			onStart: $.proxy(function () {
				// step3
				for (var i = 0, max = this.welLayerTxt.children().length; i < max; i++) {
					this.tween.layerTxt.show(this.welLayerTxt.children().eq(i));
				}
			}, this),
			onComplete: $.proxy(function () {
				this.welContainer.trigger("onOpenlayer");
			}, this),
		});
	},
	closeLayer: function (e) {
		var layerTxtHideDuration = null;
		var keyIn = false;
		if (patrasche.Cardunit.GlobalState.CurrentKeycode != null) {
			keyIn = true;
		}

		e.preventDefault();
		if (!this.opts.stateAttr.opened) return;
		this.opts.stateAttr.opened = false;
		patrasche.Cardunit.GlobalState.OpenedLayerCount--;
		if (patrasche.Cardunit.GlobalState.OpenedLayerCount < 0) patrasche.Cardunit.GlobalState.OpenedLayerCount = 0;

		layerTxtHideDuration = this.opts.time.hide.eachDelay * (this.welLayerTxt.children().length - 1) + this.opts.time.hide.duration;
		this.timelineLayer.clear().to({}, layerTxtHideDuration, {
			onStart: $.proxy(function () {
				// step1
				this.bindOutsideEvents(false);
				for (var i = 0, max = this.welLayerTxt.children().length; i < max; i++) {
					this.tween.layerTxt.hide(this.welLayerTxt.children().eq(i));
				}
			}, this),
		});
		this.timelineLayer.to({}, this.opts.time.hide.dimmedDuration, {
			onStart: $.proxy(function () {
				// step2
				this.welContainer.removeClass(this.opts.classAttr.active);
				this.welContainer.trigger("onStartCloseDimmed");
			}, this),
			onComplete: $.proxy(function () {
				if (patrasche.Cardunit.GlobalState.OpenedLayerCount <= 0 && e.type !== "clickoutside") {
					if (keyIn) {
						this.activeKeyInDelivery = keyIn;
						this.welBtnOpen.focus();
					}
				}
			}, this),
		});
		// step3
		this.timelineLayer.to(
			this.welLayer,
			0,
			$.extend(this.opts.styleAttr.hideLayer, {
				onComplete: $.proxy(function () {
					this.welContainer.trigger("onCloselayer");
				}, this),
			})
		);
	},
	onClickoutsideObj: function (e) {
		if (!this.welBtnClose.is(e.target) && this.welBtnClose.has(e.target).length === 0) {
			if ($(e.target).hasClass(this.opts.learnBtnClass)) return;
			this.closeLayer(e);
			if (this.welContainer.hasClass(this.opts.classAttr.over)) {
				this.welContainer.removeClass(this.opts.classAttr.over);
			}
		}
	},
	onMouseenterObj: function (e) {
		if (e.type === "focusin" && patrasche.Cardunit.GlobalState.CurrentKeycode === null) return;
		clearTimeout(this.timerMouseleave);
		if (!this.welContainer.hasClass(this.opts.classAttr.over)) {
			this.welContainer.addClass(this.opts.classAttr.over);
		}
	},
	onMouseleaveObj: function (e) {
		if (e.type === "focusout" && patrasche.Cardunit.GlobalState.CurrentKeycode === null) return;
		this.timerMouseleave = setTimeout(
			$.proxy(function () {
				if (this.welContainer.hasClass(this.opts.classAttr.over)) {
					this.welContainer.removeClass(this.opts.classAttr.over);
				}
			}, this),
			100
		);
	},
	onFocusTarget: function (e) {
		if (patrasche.Cardunit.GlobalState.CurrentKeycode != null || this.activeKeyInDelivery) {
			if (this.activeKeyInDelivery) this.activeKeyInDelivery = false;
			$(e.currentTarget).addClass(this.opts.classAttr.keyFocus);
		}
	},
	onBlurTarget: function (e) {
		$(e.currentTarget).removeClass(this.opts.classAttr.keyFocus);
	},
};

patrasche.Cardunit.Component = function (container, opts) {
	var defOpts = {
		// selector
		imgWrapSelector: ".content_img",
		imgFilterSelector: ".content_img_filter",
		jsCanvasSelector: ".js-canvas",
		layerSelector: ".cardunit_detail",
		svgIconSelector: ".js-svg-icon",
		autoDrawTargetSelector: ".cardunit_inner",
		// class
		showLayerClass: "is-show",
		hoverLayerClass: "is-hover",
		scrollInClass: "is-scrollin",
		// data attribute
		layerOpendData: "layer-opened",
		scrollActiveData: "scroll-active",
		autoDrawTweenData: "autodraw-tween",
		canvasFrameLengthData: "frame-length",
		canvasDurationData: "duration",
		canvasImgData: {
			PC: "pc-img",
			// MO: "mo-img",
		},
		frameSrcData: {
			PC: "pc-frame-src",
			// MO: "mo-frame-src",
		},
		blurRadiusPx: 3,
		autoDrawOpts: {},
		on: {
			loadComplete: null,
			changeViewType: null,
		},
	};
	this.opts = $.extend(defOpts, opts || {});
	if (!(this.welContainer = $(container)).length) return;
	this.init();
};
patrasche.Cardunit.Component.prototype = {
	init: function () {
		this.setElements();
		this.initOpts();
		this.loadImages().done(
			$.proxy(function () {
				this.outCallback("loadComplete");
				this.initLayout();
				this.bindEvents();
			}, this)
		);
		this.welContainer.data("cardunit", this);
	},
	setElements: function () {
		this.welImgWrap = this.welContainer.find(this.opts.imgWrapSelector);
		this.welImgFilter = this.welImgWrap.find(this.opts.imgFilterSelector);
		this.welJsCanvas = this.welImgFilter.find(this.opts.jsCanvasSelector);
		if (this.welJsCanvas.length) {
			this.welCanvas = this.welJsCanvas.find("canvas");
		}
		this.welLayer = this.welContainer.find(this.opts.layerSelector);
		this.welSvgIcon = this.welContainer.find(this.opts.svgIconSelector);
	},
	initOpts: function () {
		this.status = {};
		this.status.scrollActiveType = this.welImgWrap.data(this.opts.scrollActiveData);
		this.status.useJsCanvas = this.welJsCanvas.length ? true : false;
	},
	loadImages: function () {
		var deferred = $.Deferred();
		if (this.status.useJsCanvas) {
			new patrasche.Cardunit.CanvasLoad(this.welJsCanvas, {
				canvasFrameLengthData: this.opts.canvasFrameLengthData,
				canvasImgData: this.opts.canvasImgData,
				frameSrcData: this.opts.frameSrcData,
				on: {
					complete: $.proxy(function () {
						deferred.resolve();
					}, this),
				},
			});
		} else {
			deferred.resolve();
		}
		return deferred.promise();
	},
	initLayout: function () {
		if (this.welImgFilter.length) {
			this.buildBlurPolyfill();
		}
		if (this.welSvgIcon.length) {
			this.buildInlineSvg();
		}
		if (this.welLayer.length) {
			this.buildLayer();
		}
		if (this.status.scrollActiveType && this.status.scrollActiveType.length) {
			if (this.status.scrollActiveType === "autoDraw") {
				this.buildTween();
			}
			this.buildScrollMagic();
		}
	},
	buildBlurPolyfill: function () {
		var cssSupported = function (property, value) {
			var tempElement = document.createElement("div");
			if (!(property in tempElement.style)) return false;
			tempElement.style[property] = value;
			return tempElement.style[property] === value;
		};

		if ((this.status.useBlurPolyfill = !cssSupported("filter", "blur(" + this.opts.blurRadiusPx + "px)"))) {
			$.extend(this, {
				blurPolyfill: {
					instance: null,
					build: $.proxy(function () {
						this.blurPolyfill.instance = new patrasche.Cardunit.Blur(this.welImgFilter, {
							blurRadiusPx: this.opts.blurRadiusPx,
							imgFilterSelector: this.opts.imgFilterSelector,
							jsPictureSelector: this.opts.jsPictureSelector,
							jsCanvasSelector: this.opts.jsCanvasSelector,
							canvasImgData: this.opts.canvasImgData,
						});
					}, this),
					remove: $.proxy(function () {
						this.blurPolyfill.instance.removeSvgBlur();
					}, this),
					rebuild: $.proxy(function () {
						this.blurPolyfill.instance.rebuildSvgBlur();
					}, this),
				},
			});
			this.blurPolyfill.build();
		}
	},
	buildInlineSvg: function () {
		new patrasche.Cardunit.svgIcon(this.welSvgIcon);
	},
	buildLayer: function () {
		$.extend(this, {
			layer: {
				instance: null,
				build: $.proxy(function () {
					if (this.layer.instance !== null) return;
					this.layer.instance = new patrasche.Cardunit.LayerPopup(this.welContainer);
				}, this),
			},
		});
		this.layer.build();
	},
	buildTween: function () {
		$.extend(this, {
			autoDraw: {
				instance: null,
				images: null,
				opts: {
					to: {
						curFrame: 0,
					},
					from: {
						roundProps: "curFrame",
						paused: true,
						immediateRender: true,
					},
				},
				progress: 0,
				setSize: $.proxy(function () {
					if (this.autoDraw.images === null) {
						this.autoDraw.images = this.welCanvas.data(this.opts.canvasImgData["PC"]);
					}
					this.welCanvas.get(0).width = this.autoDraw.images[0].width;
					this.welCanvas.get(0).height = this.autoDraw.images[0].height;
				}, this),
				build: $.proxy(function () {
					var elContext2d = this.welCanvas.get(0).getContext("2d", { alpha: false }),
						dataDuration = this.welCanvas.data(this.opts.canvasDurationData),
						dataFrameLength = this.welCanvas.data(this.opts.canvasFrameLengthData);

					this.autoDraw.setSize();
					this.autoDraw.instance = new TweenLite.to(
						this.autoDraw.opts.to,
						dataDuration / 1000,
						$.extend(this.autoDraw.opts.from, {
							curFrame: parseInt(dataFrameLength - 1),
							onUpdate: $.proxy(function (self) {
								elContext2d.drawImage(this.autoDraw.images[this.autoDraw.opts.to.curFrame], 0, 0);
								this.autoDraw.progress = self.progress();
							}, this),
							onUpdateParams: ["{self}"],
						})
					);
				}, this),
				rebuild: $.proxy(function () {
					var prevProgress = this.autoDraw.progress;
					$.extend(this.autoDraw.opts.to, {
						curFrame: 0,
					});
					this.autoDraw.build();
					this.autoDraw.instance.progress(prevProgress);
				}, this),
				play: $.proxy(function () {
					this.autoDraw.instance.play();
				}, this),
				reverse: $.proxy(function () {
					this.autoDraw.instance.reverse();
				}, this),
				reset: $.proxy(function () {
					if (this.welContainer.data(this.opts.layerOpendData)) return;
					this.autoDraw.instance.pause(0, false);
				}, this),
				getProgress: $.proxy(function () {
					return this.autoDraw.instance.progress();
				}, this),
				kill: $.proxy(function () {
					this.autoDraw.instance.kill();
				}, this),
			},
		});

		$.extend(this.autoDraw.opts, this.opts.autoDrawOpts);
		this.autoDraw.build();
	},
	buildScrollMagic: function () {
		$.extend(this, {
			scrollmagic: {
				state: {
					scrollDirection: "FORWARD", // 'FORWARD', 'REVERSE', ~~'PAUSED'~~
					isEntered: null, // true, false
					isHitStart: null, // true, false
				},
				timer: null,
				controller: {
					instance: null,
				},
				scene: {
					instance: null,
					opts: {
						triggerElement: this.welContainer.get(0),
						// triggerHook: 0.5,
						offset: 0,
						duration: $(window).height() * 0.5 + this.welContainer.outerHeight(),
					},
				},
				buildController: $.proxy(function () {
					if (this.scrollmagic.controller.instance !== null) return;
					this.scrollmagic.controller.instance = new ScrollMagic.Controller();
				}, this),
				buildScene: $.proxy(function () {
					if (this.scrollmagic.controller.instance === null) return;
					this.scrollmagic.scene.instance = new ScrollMagic.Scene(this.scrollmagic.scene.opts)
						.on(
							"update",
							$.proxy(function (e) {
								var newScrollDirection = this.scrollmagic.getCurDirection(e);
								var newOffset = 0;

								if (this.scrollmagic.state.scrollDirection !== newScrollDirection) {
									this.scrollmagic.state.scrollDirection = newScrollDirection;
									newOffset = newScrollDirection === "FORWARD" ? 0 : -($(window).height() * 0.5);
									this.scrollmagic.updateOffset(newOffset);
								}

								clearTimeout(this.scrollmagic.timer);
								this.scrollmagic.timer = setTimeout(
									$.proxy(function () {
										if (newScrollDirection === "FORWARD") {
											if (this.scrollmagic.state.isEntered && this.scrollmagic.state.isHitStart) {
												// enter && start
												this.runAnimation("forward");
											} else if (!this.scrollmagic.state.isEntered && !this.scrollmagic.state.isHitStart) {
												// leave && end
												this.autoDraw.reset();
											}
										} else if (newScrollDirection === "REVERSE") {
											if (this.scrollmagic.state.isEntered && !this.scrollmagic.state.isHitStart) {
												// enter && end
												this.runAnimation("forward");
											} else if (!this.scrollmagic.state.isEntered && this.scrollmagic.state.isHitStart) {
												// leave && start
												this.autoDraw.reset();
											}
										}
									}, this),
									50
								); // FORWARD일 때: update-enter-start-progress-end-leave, REVERSE일 때: update-enter-end-progress-start-leave 라서 enter일 때 마지막 이벤트 사이클 잡기가 힘들어서 임의로 delay 함..
							}, this)
						)
						.on(
							"start end",
							$.proxy(function (e) {
								this.scrollmagic.state.isHitStart = e.type === "start" ? true : false;
							}, this)
						)
						.on(
							"enter leave",
							$.proxy(function (e) {
								this.scrollmagic.state.isEntered = e.type === "enter" ? true : false;
							}, this)
						)
						.addIndicators()
						.addTo(this.scrollmagic.controller.instance);
				}, this),
				updateOffset: $.proxy(function (newOffset) {
					this.scrollmagic.scene.instance.offset(newOffset);
				}, this),
				getCurDirection: $.proxy(function (e) {
					var curDirection = e.target.controller().info("scrollDirection") === "PAUSED" ? this.scrollmagic.state.scrollDirection : e.target.controller().info("scrollDirection").toUpperCase();
					return curDirection;
				}, this),
			},
		});
		this.scrollmagic.buildController();
		this.scrollmagic.buildScene();
	},
	bindEvents: function () {
		this.welContainer.find(this.opts.autoDrawTargetSelector).on("mouseenter focusin", $.proxy(this.runAnimation, this, "forward")).on("mouseleave focusout", $.proxy(this.runAnimation, this, "backward"));
		this.welContainer.on("onStartCloseDimmed", $.proxy(this.runAnimation, this, "backward"));
	},
	outCallback: function (ing, props) {
		var callbackObj = this.opts.on[ing];
		if (callbackObj == null) return;
		if (ing == "changeViewType") {
			callbackObj(props.viewType);
		} else {
			callbackObj();
		}
	},
	runAnimation: function (direction) {
		if (this.welContainer.hasClass(this.opts.showLayerClass)) return;

		if (direction === "forward") {
			if (this.autoDraw.getProgress() === 1) return;
			this.autoDraw.play();
		} else if (direction === "backward") {
			if (this.autoDraw.getProgress() === 0) return;
			this.autoDraw.reverse();
		}
	},
};
